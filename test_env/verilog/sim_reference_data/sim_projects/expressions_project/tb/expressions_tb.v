module expressions_tb;

   `include "setup.vh"

   `SETUP

   integer file;

initial begin
   #100;

   file = $fopen({`NAME, ".bin"}, "w");
   $display("bin: %b", `OUTPUT);
   $fwrite(file, "%b\n", `OUTPUT);
   $fclose(file);
   file = $fopen({`NAME, ".oct"}, "w");
   $display("oct: %o", `OUTPUT);
   $fwrite(file, "%o\n", `OUTPUT);
   $fclose(file);
   file = $fopen({`NAME, ".dec"}, "w");
   $display("dec: %d", `OUTPUT);
   $fwrite(file, "%d\n", `OUTPUT);
   $fclose(file);
   file = $fopen({`NAME, ".hex"}, "w");
   $display("hex: %h", `OUTPUT);
   $fwrite(file, "%h\n", `OUTPUT);
   $fclose(file);
end


endmodule