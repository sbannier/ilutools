#! /usr/bin/perl -w

use strict;

use Cwd;

my $simulator   = "modelsim";
my $machet      = cwd()."/../../tools/machet/machet.pl";
my $projectDir  = cwd()."/sim_projects/expressions_project";
my $sourceDir   = cwd()."/expressions";
my $outputDir   = cwd()."/expressions_results";
my $templateDir = cwd()."/templates";


my $currentExp;
my $path;

# Verilog testcase template
my $line1 = "`define NAME ";
my $line2 = "`define SETUP //\n";
my $line3 = "`define OUTPUT `include `NAME\n";

my $setup;

my @array;

my $testcaseTemplate = "";



if (!-d $projectDir."/testcases") {
  mkdir $projectDir."/testcases";
}

# Read all expressions and put their path in array
opendir(DIR, $sourceDir);
while ($currentExp = readdir(DIR)) {
  if (-f $sourceDir."/".$currentExp) {
    push(@array, $currentExp);
  }
}

# Sort array
@array = sort(@array);

# Create verilog testcase and JUnit test
foreach $currentExp (@array) {
  $setup = $line1."\"".$currentExp."\"\n".$line2.$line3;
  $path = $projectDir."/testcases/".$currentExp;
  if (!-d $path) {
    mkdir "$path";
  }
  open(FILE, ">", $path."/setup.vh") or die "Could not write testcase file!";
  print FILE $setup;
  close(FILE);

  $testcaseTemplate .= "    \@Test\n";
  $testcaseTemplate .= "    public void ".$currentExp."() throws Exception {\n";
  $testcaseTemplate .= "        assertTrue(testExpression(\"".$currentExp."\"));\n";
  $testcaseTemplate .= "    }\n\n";
}

# Write JUnit tests in template file
if (!-d $templateDir) {
  mkdir $templateDir;
}
open(FILE, ">", $templateDir."/expressionTestcaseTemplate.txt") or die "Could not write testcase file!";
print FILE $testcaseTemplate;
close(FILE);

# Run simulation
if (!-d $projectDir."/sim") {
  mkdir $projectDir."/sim";
}
chdir $projectDir."/sim";
system $machet." --tool ".$simulator." --clean";
foreach (@array) {
  system($machet." --tool ".$simulator." --nodump --testcase ".$_);
}

# Copy all simulation results
system "rm ".$outputDir."/*.bin";
system "rm ".$outputDir."/*.oct";
system "rm ".$outputDir."/*.dec";
system "rm ".$outputDir."/*.hex";
if (!-d $outputDir) {
  mkdir $outputDir;
}
system "cp ".$simulator."/*.bin ".$outputDir;
system "cp ".$simulator."/*.oct ".$outputDir;
system "cp ".$simulator."/*.dec ".$outputDir;
system "cp ".$simulator."/*.hex ".$outputDir;

chdir "../../..";

