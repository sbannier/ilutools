###############################################################################
# Title      : Machet script
# Project    : machet flow
###############################################################################
# File       : machet_config.pl
# Author     : Sascha Bannier
# Company    : ---
###############################################################################
# Description: Machet design flow file location settings
###############################################################################
# Copyright (c) 2011   Sascha Bannier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
# Revisions  :
# Date        Author           Description
# 2011-07-21  Sascha Bannier   Created
# 2011-08-04  Sascha Bannier   Added sub routines for mode selection and paths
# 2011-08-08  Sascha Bannier   Changed typo (pathes to path, how embarrassing)
###############################################################################



use strict;


my $defaultCfgName = "machet.cfg";                         # Default config file name


my $fpgaDirName = "fpga";                                  # Name of the directory in which FPGA synthesis is started
my $fpgaDirToProjectBase = "/../../..";                    # Path from FPGA synthesis directory to project base directory
my $fpgaDirToDefaultCfg = "/..";                           # Path from FPGA synthesis directory to default location of cfg files


my $simDirName  = "sim";                                   # Name of the directory in which simulation is started
my $simDirToProjectBase = "/../../..";                     # Path from simulation directory to project base directory
my $simDirToDefaultCfg = "/..";                            # Path from simulation directory to default location of cfg files




# |-----------------------------------------------------------------------------
# | Returns the selected mode based on the directory in which the script has
# | been started
# |-----------------------------------------------------------------------------
sub getMode {

  my $callDir = $MAIN::CALLDIR;

  # FPGA mode
  if ($callDir =~m/\/($fpgaDirName)$/) {
    return "FPGA";
  }
  # SIM mode
  elsif ($callDir =~m/\/($simDirName)$/) {
    return "SIM";
  }
  else {
    print "ERROR: Script startet in wrong directory! Use \"--help\" to show help screen.\n";
    exit(-1);
  }

}



# |-----------------------------------------------------------------------------
# | Returns the full project base directory path based on the selected mode and
# | the directory in which the script has been started
# |-----------------------------------------------------------------------------
sub getProjectBase {

  my $mode = $MAIN::MODE;
  my $callDir = $MAIN::CALLDIR;

  if ($mode eq 'FPGA') {
    return cleanPath($callDir.$fpgaDirToProjectBase);
  }
  elsif ($mode eq 'SIM') {
    return cleanPath($callDir.$simDirToProjectBase);
  }
  else {
    die("getProjectBase: somehow \$MAIN::MODE is messed up.\n");
  }

}



# |-----------------------------------------------------------------------------
# | Returns the absolute path to the default config file
# |-----------------------------------------------------------------------------
sub getDefaultCfgLocation {

  my $mode = $MAIN::MODE;
  my $callDir = $MAIN::CALLDIR;

  if ($mode eq 'FPGA') {
    return cleanPath($callDir.$fpgaDirToDefaultCfg."/".$defaultCfgName);
  }
  elsif ($mode eq 'SIM') {
    return cleanPath($callDir.$simDirToDefaultCfg."/".$defaultCfgName);
  }
  else {
    die("getDefaultCfgLocation: somehow \$MAIN::MODE is messed up.\n");
  }

}


# Dummy
1;
