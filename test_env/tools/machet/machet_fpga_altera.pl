###############################################################################
# Title      : Machet Altera FPGA flow
# Project    : machet flow
###############################################################################
# File       : machet_fpga_altera.pl
# Author     : Sascha Bannier
# Company    : ---
###############################################################################
# Description: Machet sub script for FPGA synthesis using Altera tools
###############################################################################
# Copyright (c) 2011   Sascha Bannier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
# Revisions  :
# Date        Author           Description
# 2011-02-10  Sascha Bannier   Created
# 2011-02-21  Sascha Bannier   Split script generation and execution
# 2011-02-24  Sascha Bannier   Added return values
# 2011-04-20  Sascha Bannier   Added constraint support
#                              Uses expandToAbsolutePath subroutine
# 2011-05-06  Sascha Bannier   Beautifications
# 2011-06-10  Sascha Bannier   quartus_sh used wrong file because $fileloc was
#                              overwritten
# 2011-08-08  Sascha Bannier   Changed typo (pathes to path, how embarrassing)
###############################################################################


use strict;


sub fpga_altera {

  # Variable from MAIN
  my $VERBOSE              = $MAIN::VERBOSE; # Enable full comments
  my $DRY                  = $MAIN::DRY; # Perform dry run only, creates only scripts
  my $SYN                  = $MAIN::SYN; # Command line option for synthesis is set
  my $PAR                  = $MAIN::PAR; # Command line option for place and route is set
  my $TIMING               = $MAIN::TIMING; # Command line option for timing analysis is set

  my $CALLDIR              = $MAIN::CALLDIR; # Directory from where this script is called
  my $PROJECTBASE          = $MAIN::PROJECTBASE; # Base directory of project
  my @IMP_NAME             = @MAIN::IMP_NAME; # Implementation name
  my @INCLUDE_PATHS        = @MAIN::INCLUDE_PATHS; # List of include files (pointing to files directly)
  my @VERILOG_DEFINES      = @MAIN::VERILOG_DEFINES; # Verilog defines used for all files
  my @TOPLEVEL             = @MAIN::TOPLEVEL; # Top level module name for FPGA or SIM run
  my @FPGAPART             = @MAIN::FPGAPART; # Part type of target FPGA

  my @SOURCEFILE_LIST      = @MAIN::SOURCEFILE_LIST;          # Complete list of all source files with absolute path

  my @CONSTRAINT_FILES     = @MAIN::ALT_CONSTRAINT_FILES; # List of synthesis constraint files for FPGA run (pointing to files directly)
  my @SDC_FILES            = @MAIN::ALT_SDC_FILES; # List of synthesis constraint files for FPGA timing analysis (pointing to files directly)


  # Local variables
  my $TCL_FILE = '';            # TCL file for project generation
  my $PROJGEN_COMMAND = '';     # Quartus command to create project
  my $MAP_COMMAND = '';         # Command to start QUARTUS_MAP
  my $FIT_COMMAND = '';         # Command to start QUARTUS_FIT
  my $ASM_COMMAND = '';         # Command to start QUARTUS_ASM
  my $STA_COMMAND = '';         # Command to start QUARTUS_STA

  my $fileloc;                  # Location of file
  my $tclFileLoc;               # Location of TCL file for project creation
  my $line;                     # Single line
  my $ret = 1;                  # Return value


  # Create quartus directory if not present
  unless (-d convPath($CALLDIR."/quartus")) {
    systemCall("mkdir quartus");
  }



  # ----------------------------------------------------------------------------
  #Build Quartus II project TCL file
  print "/-----------------------------------------------------------------------------\\\n";
  print "| Building TCL script for project file generation                             |\n";
  print "\\-----------------------------------------------------------------------------/\n";

  # Load packages
  $TCL_FILE = "load_package flow\n\n";

  # Create project
  $TCL_FILE .= "project_new ".$IMP_NAME[0]." -overwrite\n\n";

  # Set toplevel
  $TCL_FILE .= "set_global_assignment -name TOP_LEVEL_ENTITY ".$TOPLEVEL[0]."\n\n";

  # Set FPGA part
  $TCL_FILE .= "set_global_assignment -name DEVICE ".$FPGAPART[0]."\n\n";

  # Includes
  foreach (@INCLUDE_PATHS) {
    $TCL_FILE .= 'set_global_assignment -name SEARCH_PATH "'.$_.'"'."\n\n";
  }

  $TCL_FILE .= "\n";

  # Check file type of all source files in list. Remove every file from
  # beginning of list, check if file type is supported, add entry to TCL
  # file and put file back to end of source file list. Not supported file
  # types are removed from list
  foreach (1..@SOURCEFILE_LIST) {
    # Remove first entry from source file list
    $line = shift(@SOURCEFILE_LIST);

    # Verilog
    if ($line =~ m/\.v$/i) {
      push(@SOURCEFILE_LIST, $line);
      $line = 'set_global_assignment -name VERILOG_FILE "'.$line.'"'."\n";
      $TCL_FILE .= $line;
    }
    # VHDL
    elsif ($line =~ m/\.vhd$/i) {
      push(@SOURCEFILE_LIST, $line);
      $line = 'set_global_assignment -name VHDL_FILE "'.$line.'"'."\n";
      $TCL_FILE .= $line;
    }
      # Something
    else {
      printf ("Warning: \"%s\" is unsupported file type\n", convPath($line));
    }
  }
  $TCL_FILE .= "\n\n";

  # Constraints
  foreach (1..@CONSTRAINT_FILES) {
    # Take first entry and check if file exists
    $line = shift(@CONSTRAINT_FILES);
    # Expand to absolute path
    $fileloc = expandToAbsolutePath($line);
    # Check if file exists
    if (-f $fileloc) {
      push(@CONSTRAINT_FILES, $fileloc);
    }
    else {
      printf ("Warning: \"%s\" does not exist\n", convPath($fileloc));
    }
  }

  # Report
  if ($VERBOSE == 1) {
    print "  Constraint files read:\n";
    foreach (@CONSTRAINT_FILES) {
      printf ("    %s\n", convPath($_));
    }
  }

  # Append constraints to TCL file
  foreach $fileloc (@CONSTRAINT_FILES) {
    # Header
    $TCL_FILE .= "################################################################################\n";
    $TCL_FILE .= "# Constraints taken from ".convPath($fileloc)."\n";
    $TCL_FILE .= "################################################################################\n";
    # Read file and append whole content to single file
    open(CONFILE, "<", $fileloc) or die "Could not open constraint file!";
    # Read file
    while (defined($line = <CONFILE>)) {
      $TCL_FILE .= $line;
    }
    close(CONFILE);
    $TCL_FILE .= "\n\n";
  }

  # Timing contraints
  foreach (1..@SDC_FILES) {
    # Take first entry and check if file exists
    $line = shift(@SDC_FILES);
    # Expand to absolute path
    $fileloc = expandToAbsolutePath($line);
    # Check if file exists
    if (-f $fileloc) {
      push(@SDC_FILES, $fileloc);
      $TCL_FILE .= 'set_global_assignment -name SDC_FILE "'.$fileloc.'"'."\n";
    }
    else {
      printf ("Warning: \"%s\" does not exist\n", convPath($fileloc));
    }
  }

  # Close project
  $TCL_FILE .= "project_close\n";


  # Write TCL file
  $tclFileLoc = convPath($CALLDIR."/".$IMP_NAME[0].".tcl");
  open(TCLFILE, ">", $tclFileLoc) or die "Could not write tcl project file";
  print TCLFILE $TCL_FILE;
  close(TCLFILE);
  if ($VERBOSE == 1) {
    printf ("  Wrote \"%s\"\n", $tclFileLoc);
  }




  # ----------------------------------------------------------------------------
  # Build synthesis scripts
  if ($SYN == 1) {
    print "/-----------------------------------------------------------------------------\\\n";
    print "| Building script files for QUARTUS_MAP                                       |\n";
    print "\\-----------------------------------------------------------------------------/\n";
    $MAP_COMMAND = "quartus_map ".$IMP_NAME[0];

    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".alt.map");
    writeBatch($fileloc, "quartus", $MAP_COMMAND);
  }



  # ----------------------------------------------------------------------------
  # Build place and route scripts
  if ($PAR == 1) {
    print "/-----------------------------------------------------------------------------\\\n";
    print "| Building script files for QUARTUS_FIT and QUARTUS_ASM                       |\n";
    print "\\-----------------------------------------------------------------------------/\n";
    $FIT_COMMAND = "quartus_fit ".$IMP_NAME[0];
    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".alt.fit");
    writeBatch($fileloc, "quartus", $FIT_COMMAND);

    $ASM_COMMAND = "quartus_asm ".$IMP_NAME[0];
    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".alt.asm");
    writeBatch($fileloc, "quartus", $ASM_COMMAND);
  }



  # ----------------------------------------------------------------------------
  # Build timing analysis scripts
  if ($TIMING == 1) {
    print "/-----------------------------------------------------------------------------\\\n";
    print "| Building script files for QUARTUS_STA                                       |\n";
    print "\\-----------------------------------------------------------------------------/\n";
    $STA_COMMAND = "quartus_sta ".$IMP_NAME[0];
    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".alt.sta");
    writeBatch($fileloc, "quartus", $STA_COMMAND);
  }




  # ----------------------------------------------------------------------------
  # Run commands
  unless ($DRY == 1) {

    # Create Quartus II project
    print "/-----------------------------------------------------------------------------\\\n";
    print "| Building QUARTUS II project file                                            |\n";
    print "\\-----------------------------------------------------------------------------/\n";
    $PROJGEN_COMMAND = "quartus_sh -t ".convPath($tclFileLoc);
    # Start QUARTUS_SH to create project file
    chdir("quartus");
    $ret = systemCall($PROJGEN_COMMAND);
    chdir("..");
    if ($ret != 0) {
      return($ret);
    }

    # Run synthesis
    if ($SYN == 1) {
      print "/-----------------------------------------------------------------------------\\\n";
      print "| Starting QUARTUS_MAP                                                        |\n";
      print "\\-----------------------------------------------------------------------------/\n";
      #Start QUARTUS_MAP and stop on error
      chdir("quartus");
      $ret = systemCall($MAP_COMMAND);
      chdir("..");
      if ($ret != 0) {
        return($ret);
      }
    }

    # Run place and route
    if ($PAR == 1) {
      print "/-----------------------------------------------------------------------------\\\n";
      print "| Starting QUARTUS_FIT                                                        |\n";
      print "\\-----------------------------------------------------------------------------/\n";
      #Start QUARTUS_FIT and stop on error
      chdir("quartus");
      $ret = systemCall($FIT_COMMAND);
      chdir("..");
      if ($ret != 0) {
        return($ret);
      }

      print "/-----------------------------------------------------------------------------\\\n";
      print "| Starting QUARTUS_ASM                                                        |\n";
      print "\\-----------------------------------------------------------------------------/\n";
      #Start QUARTUS_ASM and stop on error
      chdir("quartus");
      $ret = systemCall($ASM_COMMAND);
      chdir("..");
      if ($ret != 0) {
        return($ret);
      }
    }

    # Run timing analysis
    if ($TIMING == 1) {
      print "/-----------------------------------------------------------------------------\\\n";
      print "| Starting QUARTUS_STA                                                        |\n";
      print "\\-----------------------------------------------------------------------------/\n";
      #Start QUARTUS_STA and stop on error
      chdir("quartus");
      $ret = systemCall($STA_COMMAND);
      chdir("..");
      if ($ret != 0) {
        return($ret);
      }
    }
  }

  return($ret);

}



# Dummy
1;
