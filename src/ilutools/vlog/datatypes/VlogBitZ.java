// ILUtools - A set of hardware description language tools
// Copyright (C) 2012  Sascha Bannier 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ilutools.vlog.datatypes;



/**
 * This class represents a single bit set to the value 1'bz in Verilog syntax.
 * It is implemented as a singleton.
 * @author sbannier
 */
public final class VlogBitZ extends VlogBit {

    /**
     * Returns a reference to the singleton object
     * @return  Reference to singleton object
     */
    public static VlogBitZ getInstance() {
        return instance;        
    }
    
    /**
     * Static class variable storing the reference to the singleton object
     */
    private static VlogBitZ instance = new VlogBitZ();
    
    /**
     * Private constructor
     */
    private VlogBitZ() {
    }

    
    

    @Override
    public boolean isZero() {
        return false;
    }
    
    @Override
    public boolean isOne() {
        return false;
    }

    @Override
    public boolean isX() {
        return false;
    }

    @Override
    public boolean isZ() {
        return true;
    }

    
    
    
    @Override
    public VlogBit and(VlogBit bit) {
        if (bit == VlogBitZero.getInstance()) {
            return VlogBitZero.getInstance();
        }
        else {
            return VlogBitX.getInstance();
        }
    }

    @Override
    public VlogBit or(VlogBit bit) {
        if (bit == VlogBitOne.getInstance()) {
            return VlogBitOne.getInstance();
        }
        else {
            return VlogBitX.getInstance();
        }
    }
    
    @Override
    public VlogBit xor(VlogBit bit) {
        return VlogBitX.getInstance();
    }
    
    
    @Override
    public VlogBit not() {
        return VlogBitX.getInstance();
    }

    
    
    
    
    
    
    
    
    
    
    
    @Override
    public String toString() {
        return "z";
    }

}
