// ILUtools - A set of hardware description language tools
// Copyright (C) 2012  Sascha Bannier 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ilutools.vlog.datatypes;



/**
 * This class represents a single bit set to the value 1'b0 in Verilog syntax.
 * It is implemented as a singleton.
 * @author sbannier
 */
public final class VlogBitZero extends VlogBit {

    /**
     * Returns a reference to the singleton object
     * @return  Reference to singleton object
     */
    public static VlogBitZero getInstance() {
        return instance;        
    }
    
    /**
     * Static class variable storing the reference to the singleton object
     */
    private static VlogBitZero instance = new VlogBitZero();
    
    /**
     * Private constructor
     */
    private VlogBitZero() {
    }

    

    @Override
    public boolean isZero() {
        return true;
    }
    
    @Override
    public boolean isOne() {
        return false;
    }

    @Override
    public boolean isX() {
        return false;
    }

    @Override
    public boolean isZ() {
        return false;
    }

    
    
    
    
    
    @Override
    public VlogBit and(VlogBit bit) {
        return VlogBitZero.getInstance();
    }
    
    @Override
    public VlogBit or(VlogBit bit) {
        if (bit == VlogBitZero.getInstance()) {
            return VlogBitZero.getInstance();
        }
        else if (bit == VlogBitOne.getInstance()) {
            return VlogBitOne.getInstance();
        }
        else {
            return VlogBitX.getInstance();
        }
    }
    
    @Override
    public VlogBit xor(VlogBit bit) {
        if (bit == VlogBitZero.getInstance()) {
            return VlogBitZero.getInstance();
        }
        else if (bit == VlogBitOne.getInstance()) {
            return VlogBitOne.getInstance();
        }
        else {
            return VlogBitX.getInstance();
        }    
    }
    
    @Override
    public VlogBit not() {
        return VlogBitOne.getInstance();
    }

    
    
    
    
    
    
    @Override
    public String toString() {
        return "0";
    }

}
