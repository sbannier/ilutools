// ILUtools - A set of hardware description language tools
// Copyright (C) 2012  Sascha Bannier 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ilutools.vlog.datatypes;



/**
 * The class VlogBit is the super class used to store Verilog's four state logic of integer values.
 * The classes VlogBitZero, VlogBitOne, VlogBitX and VlogBitZ are sub-classes of this class and represent the possible bit values. 
 * @author sbannier
 */
public abstract class VlogBit {

    /**
     * Check if value of this bit is 1'b0
     * @return boolean true if value is 1'b0
     */
    public abstract boolean isZero();
    
    /**
     * Check if value of this bit is 1'b1
     * @return boolean true if value is 1'b1
     */
    public abstract boolean isOne();
    
    /**
     * Check if value of this bit is 1'bx
     * @return boolean true if value is 1'bx
     */
    public abstract boolean isX();
    
    /**
     * Check if value of this bit is 1'bz
     * @return boolean true if value is 1'bz
     */
    public abstract boolean isZ();
    
    
    /**
     * Calculates the result of a logic 'and' operation between this bit and another bit
     * @param bit  Bit to perform logic 'and' operation with
     * @return  A VlogBit with the value of the result of the above operation
     */
    public abstract VlogBit and(VlogBit bit);
    
    /**
     * Calculates the result of a logic 'or' operation between this bit and another bit
     * @param bit  Bit to perform logic 'or' operation with
     * @return  A VlogBit with the value of the result of the above operation
     */
    public abstract VlogBit or(VlogBit bit);
    
    /**
     * Calculates the result of a logic 'xor' operation between this bit and another bit
     * @param bit  Bit to perform logic 'xor' operation with
     * @return  A VlogBit with the value of the result of the above operation
     */
    public abstract VlogBit xor(VlogBit bit);
    
    /**
     * Calculates the result of a logic 'not' operation between this bit and another bit
     * @param bit  Bit to perform logic 'not' operation with
     * @return  A VlogBit with the value of the result of the above operation
     */
    public abstract VlogBit not();
    
    public abstract String toString();
    
}
