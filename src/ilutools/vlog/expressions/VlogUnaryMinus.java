// ILUtools - A set of hardware description language tools
// Copyright (C) 2012  Sascha Bannier 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ilutools.vlog.expressions;

import ilutools.vlog.interfaces.VlogEvaluable;
import ilutools.vlog.datatypes.VlogType;
import ilutools.vlog.datatypes.VlogValue;



public final class VlogUnaryMinus extends VlogUnaryExpression implements VlogEvaluable {

    /**
     * This constructor calls the super class constructor which initializes the operand.
     * @param operand  Operand used in unary expression
     */
    public VlogUnaryMinus(VlogEvaluable operand) {
        super(operand);
    } 
    
    @Override
    public VlogValue evaluate(int desiredSize, VlogType desiredType) {
        return operand.evaluate(desiredSize, desiredType).negate();
    }

    @Override
    public int evaluateSize() {
        return operand.evaluateSize();
    }

    @Override
    public VlogType evaluateType() {
        return operand.evaluateType();
    }


    
    @Override
    public String toString() {
        return "(-" + operand.toString() + ")";
    }

}
