// ILUtools - A set of hardware description language tools
// Copyright (C) 2012  Sascha Bannier 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ilutools.vlog.expressions;

import ilutools.vlog.interfaces.VlogEvaluable;
import ilutools.vlog.datatypes.VlogType;
import ilutools.vlog.datatypes.VlogTypeUnsigned;
import ilutools.vlog.datatypes.VlogTypeSigned;
import ilutools.vlog.datatypes.VlogValue;



public final class VlogBinaryBitwiseAnd extends VlogBinaryExpression implements VlogEvaluable{

    /**
     * This constructor calls the super class constructor which initializes the operands.
     * @param operand1  First operand used in binary expression
     * @param operand2  Second operand used in binary expression
     */
    public VlogBinaryBitwiseAnd(VlogEvaluable operand1, VlogEvaluable operand2) {
        super(operand1, operand2);
    }
    
    @Override
    public VlogValue evaluate(int desiredSize, VlogType desiredType) {
        // Both operands are context-determined
        return operand1.evaluate(desiredSize, desiredType).bitwiseAnd(operand2.evaluate(desiredSize, desiredType));
    }

    @Override
    public int evaluateSize() {
        return Math.max(operand1.evaluateSize(), operand2.evaluateSize());
    }

    @Override
    public VlogType evaluateType() {
        // TODO: what happens with real?
        if (operand1.evaluateType().isUnsigned() || operand2.evaluateType().isUnsigned()) {
            return VlogTypeUnsigned.getInstance();
        }
        else {
            return VlogTypeSigned.getInstance();
        }
    }

    

    @Override
    public String toString() {
        return "(" + operand1.toString() + " & " + operand2.toString() + ")";
    }
    
}
