// ILUtools - A set of hardware description language tools
// Copyright (C) 2012  Sascha Bannier 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ilutools.vlog.expressions;

import ilutools.vlog.interfaces.VlogEvaluable;



public abstract class VlogBinaryExpression extends VlogExpression implements VlogEvaluable {

    /**
     * This constructor is implemented to force all sub-classes to implement their own constructors
     * @param operand1  First operand used in binary expression
     * @param operand2  Second operand used in binary expression
     */
    public VlogBinaryExpression(VlogEvaluable operand1, VlogEvaluable operand2) {
        this.operand1 = operand1;
        this.operand2 = operand2;
    }
    
    /**
     * First operand
     */
    protected VlogEvaluable operand1;
    /**
     * Second operand    
     */
    protected VlogEvaluable operand2;

}
