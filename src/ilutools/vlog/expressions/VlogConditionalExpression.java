// ILUtools - A set of hardware description language tools
// Copyright (C) 2012  Sascha Bannier 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ilutools.vlog.expressions;

import ilutools.vlog.interfaces.VlogEvaluable;
import ilutools.vlog.datatypes.VlogType;
import ilutools.vlog.datatypes.VlogValue;



public final class VlogConditionalExpression extends VlogExpression implements VlogEvaluable {

    /**
     * Constructor initializes the operands
     * @param operand1  First operand used in conditional expression
     * @param operand2  Second operand used in conditional expression
     * @param operand3  Third operand used in conditional expression
     */
    public VlogConditionalExpression(VlogEvaluable operand1, VlogEvaluable operand2, VlogEvaluable operand3) {
        this.operand1 = operand1;
        this.operand2 = operand2;
        this.operand3 = operand3;
        
    }

    /**
     * First operand
     */
    private VlogEvaluable operand1;
    /**
     * Second operand
     */
    private VlogEvaluable operand2;
    /**
     * Third operand
     */
    private VlogEvaluable operand3;

    
    
    
    
    
    
    
    
    
    
    
    @Override
    public VlogValue evaluate(int desiredSize, VlogType desiredType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int evaluateSize() {
        // TODO: this seems to be wrong
        return 0;
    }

    @Override
    public VlogType evaluateType() {
        // TODO: code
        return null;
    }
    
    
    @Override
    public String toString() {
        return "(" + operand1.toString() + " ? " + operand2.toString() + " : " + operand3.toString() + ")"; 
    }

    
}
