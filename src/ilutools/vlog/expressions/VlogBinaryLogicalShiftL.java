// ILUtools - A set of hardware description language tools
// Copyright (C) 2012  Sascha Bannier 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ilutools.vlog.expressions;

import ilutools.vlog.interfaces.VlogEvaluable;
import ilutools.vlog.datatypes.VlogType;
import ilutools.vlog.datatypes.VlogValue;



public final class VlogBinaryLogicalShiftL extends VlogBinaryExpression implements VlogEvaluable{

    /**
     * This constructor calls the super class constructor which initializes the operands.
     * @param operand1  First operand used in binary expression
     * @param operand2  Second operand used in binary expression
     */
    public VlogBinaryLogicalShiftL(VlogEvaluable operand1, VlogEvaluable operand2) {
        super(operand1, operand2);
    }
    
    @Override
    public VlogValue evaluate(int desiredSize, VlogType desiredType) {
        // Shift length (operand2) is self-determined
        int size = operand2.evaluateSize();
        VlogType type = operand2.evaluateType();
        VlogValue shiftLength = operand2.evaluate(size, type);
        
        return operand1.evaluate(desiredSize, desiredType).logShiftL(shiftLength);
    }

    @Override
    public int evaluateSize() {
        return operand1.evaluateSize();   
    }

    @Override
    public VlogType evaluateType() {
        return operand1.evaluateType();
    }

    
    
    @Override
    public String toString() {
        return "(" + operand1.toString() + " << " + operand2.toString() + ")";
    }
    
}
