#! /usr/bin/perl -w

use strict;
use File::Spec;

my @files = getFileList();
my @header = getHeader();

my $mismatchCount = 0;

# Move through all files
foreach my $file (@files) {

  my $mismatch = 0;

  open(my $fileReference, "<", $file) or die "Could not open Java source file";

  # Move through all header lines
  foreach my $headerLine (@header) {
    # Read new line from source file (if exists) and compare with expected header
    # Set flag on mismatch
    if (defined(my $sourceLine = <$fileReference>)) {
      chomp($sourceLine);
      unless ($sourceLine =~ m|^$headerLine\s*$|) {
        $mismatch = 1;
      }
    }
    else {
      $mismatch = 1;
    }
  }

  # Print info on mismatch
  if ($mismatch == 1) {
    print "Missing or wrong header in file $file\n";
    $mismatchCount++;
  }

  close($fileReference);

}

if ($mismatchCount == 0) {
  print "All ".scalar(@files)." java source file headers in src directory are OK.\n";
  exit(0);
}
else {
  print "Found $mismatchCount missing or wrong headers in ".scalar(@files)." java source files in src directory.\n";
  exit(-1);
}










###############################################################################
# Returns a list of all Java files from all sub directories
###############################################################################
sub getFileList {

  my $dir;
  my $currentItem;
  my @files;

  # Read all items from current directory
  opendir($dir, ".");
  while ($currentItem = readdir($dir)) {
    # Ignore . and .. directory entries
    if (($currentItem eq ".") || ($currentItem eq "..")) {
    }

    # Check if is file that ends with .java
    elsif (-f $currentItem) {
      if ($currentItem =~ m/\.java$/i) {
        push(@files, File::Spec->rel2abs($currentItem));
      }
    }

    # Check if it is directory -> change to directory and call sub recursively
    elsif (-d $currentItem) {
      chdir($currentItem);
      push(@files, getFileList());
      chdir("..");
    }
  }
  closedir($dir);

  return @files;
}



###############################################################################
# Returns the standard file header
###############################################################################
sub getHeader {

  my $fileReference;
  my @header;

  # Read header text from file
  open($fileReference, "<", "../license/file_header.txt") or die "Could not open file_header.txt in ../license";
  while (defined(my $line = <$fileReference>)) {
    chomp($line);
    push(@header, $line);
  }

  # Replace some parts for regular expression check
  foreach (1..@header) {
    my $line = shift(@header);
    # Replace '(' with '\('
    $line =~ s/\(/\\\(/;
    # Replace ')' with '\)'
    $line =~ s/\)/\\\)/;
    # Replace 'YYYY' with '20[0-9]{2}'
    $line =~ s/YYYY/20[0-9]{2}/;
    push(@header, $line);
  }

  return @header;
}
