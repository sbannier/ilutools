// ILUtools - A set of hardware description language tools
// Copyright (C) 2012  Sascha Bannier 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

grammar VlogAst;

options {
   language     = Java;
   output       = AST;
   ASTLabelType = CommonTree;
}

tokens {
   K_ALWAYS              = 'always';
   K_AND                 = 'and';
   K_ASSIGN              = 'assign';
   K_AUTOMATIC           = 'automatic';
   K_BEGIN               = 'begin';
   K_BUF                 = 'buf';
   K_BUFIF0              = 'bufif0';
   K_BUFIF1              = 'bufif1';
   K_CASE                = 'case';
   K_CASEX               = 'casex';
   K_CASEZ               = 'casez';
   K_CELL                = 'cell';
   K_CMOS                = 'cmos';
   K_CONFIG              = 'config';
   K_DEASSIGN            = 'deassign';
   K_DEFAULT             = 'default';
   K_DEFPARAM            = 'defparam';
   K_DESIGN              = 'design';
   K_DISABLE             = 'disable';
   K_EDGE                = 'edge';
   K_ELSE                = 'else';
   K_END                 = 'end';
   K_ENDCASE             = 'endcase';
   K_ENDCONFIG           = 'endconfig';
   K_ENDFUNCTION         = 'endfunction';
   K_ENDGENERATE         = 'endgenerate';
   K_ENDMODULE           = 'endmodule';
   K_ENDPRIMITIVE        = 'endprimitive';
   K_ENDSPECIFY          = 'endspecify';
   K_ENDTABLE            = 'endtable';
   K_ENDTASK             = 'endtask';
   K_EVENT               = 'event';
   K_FOR                 = 'for';
   K_FORCE               = 'force';
   K_FOREVER             = 'forever';
   K_FORK                = 'fork';
   K_FUNCTION            = 'function';
   K_GENERATE            = 'generate';
   K_GENVAR              = 'genvar';
   K_HIGHZ0              = 'highz0';
   K_HIGHZ1              = 'highz1';
   K_IF                  = 'if';
   K_IFNONE              = 'ifnone';
   K_INCDIR              = 'incdir';
   K_INCLUDE             = 'include';
   K_INITIAL             = 'initial';
   K_INOUT               = 'inout';
   K_INPUT               = 'input';
   K_INSTANCE            = 'instance';
   K_INTEGER             = 'integer';
   K_JOIN                = 'join';
   K_LARGE               = 'large';
   K_LIBLIST             = 'liblist';
   K_LIBRARY             = 'library';
   K_LOCALPARAM          = 'localparam';
   K_MACROMODULE         = 'macromodule';
   K_MEDIUM              = 'medium';
   K_MODULE              = 'module';
   K_NAND                = 'nand';
   K_NEGEDGE             = 'negedge';
   K_NMOS                = 'nmos';
   K_NOR                 = 'nor';
   K_NOSHOWCANCELLED     = 'noshowcancelled';
   K_NOT                 = 'not';
   K_NOTIF0              = 'notif0';
   K_NOTIF1              = 'notif1';
   K_OR                  = 'or';
   K_OUTPUT              = 'output';
   K_PARAMETER           = 'parameter';
   K_PMOS                = 'pmos';
   K_POSEDGE             = 'posedge';
   K_PRIMITIVE           = 'primitive';
   K_PULL0               = 'pull0';
   K_PULL1               = 'pull1';
   K_PULLDOWN            = 'pulldown';
   K_PULLUP              = 'pullup';
   K_PULSESTYLE_ONEVENT  = 'pulsestyle_onevent';
   K_PULSESTYLE_ONDETECT = 'pulsestyle_ondetect';
   K_RCMOS               = 'rcmos';
   K_REAL                = 'real';
   K_REALTIME            = 'realtime';
   K_REG                 = 'reg';
   K_RELEASE             = 'release';
   K_REPEAT              = 'repeat';
   K_RNMOS               = 'rnmos';
   K_RPMOS               = 'rpmos';
   K_RTRAN               = 'rtran';
   K_RTRANIF0            = 'rtranif0';
   K_RTRANIF1            = 'rtranif1';
   K_SCALARED            = 'scalared';
   K_SHOWCANCELLED       = 'showcancelled';
   K_SIGNED              = 'signed';
   K_SMALL               = 'small';
   K_SPECIFY             = 'specify';
   K_SPECPARAM           = 'specparam';
   K_STRONG0             = 'strong0';
   K_STRONG1             = 'strong1';
   K_SUPPLY0             = 'supply0';
   K_SUPPLY1             = 'supply1';
   K_TABLE               = 'table';
   K_TASK                = 'task';
   K_TIME                = 'time';
   K_TRAN                = 'tran';
   K_TRANIF0             = 'tranif0';
   K_TRANIF1             = 'tranif1';
   K_TRI                 = 'tri';
   K_TRI0                = 'tri0';
   K_TRI1                = 'tri1';
   K_TRIAND              = 'triand';
   K_TRIOR               = 'trior';
   K_TRIREG              = 'trireg';
   K_UNSIGNED            = 'unsigned';
   K_USE                 = 'use';
   K_UWIRE               = 'uwire';
   K_VECTORED            = 'vectored';
   K_WAIT                = 'wait';
   K_WAND                = 'wand';
   K_WEAK0               = 'weak0';
   K_WEAK1               = 'weak1';
   K_WHILE               = 'while';
   K_WIRE                = 'wire';
   K_WOR                 = 'wor';
   K_XNOR                = 'xnor';
   K_XOR                 = 'xor';
   
   ATTRIBUTE;
   
   CONST;        // Marks expressions as constant
 
   CONCAT;       // Concatenation
   MULT_CONCAT;  // Multiple concatenation
   
   RANGE;        // All kinds of ranges
   
   ID;
   HIERARCHICAL_ID;
   
   MODULE_INSTANCE;    // Instance of a module
//   MODULE_PARAMETER;   // Parameter override of a module instance
//   MODULE_PORT;        // Port connection of a module instance
   PORT_CONNECTIONS;
   PARAMETER_ASSIGNMENTS;
   ORDERED;      // Ordered port / parameter assignments
   NAMED;        // Named port / parameter assignments

}





@header {
   //------------------------------------------------------------------
   // THIS FILE HAS BEEN GENERATED BY ANTLR --- DO NOT EDIT!!!
   //------------------------------------------------------------------

   package ilutools.vlog.parser;
}

@lexer::header {
   //------------------------------------------------------------------
   // THIS FILE HAS BEEN GENERATED BY ANTLR --- DO NOT EDIT!!!
   //------------------------------------------------------------------

   package ilutools.vlog.parser;
}

@members {
   int constant_expression_cnt = 0;
   int generate_expression_cnt = 0;
}





// --------------------------------------------------------------------------
// A.1 Source text

// ------------------------------------------------------
// A.1.1 Library source text

library_text:
     library_description*
;

library_description:
     library_declaration
   | include_statement
   | config_declaration
;

library_declaration:
     K_LIBRARY library_identifier file_path_spec (',' file_path_spec)* ('-' K_INCDIR file_path_spec (',' file_path_spec)*)? ';'  // TODO: check if space is allowed between '-' and K_INCDIR
;

include_statement:
     K_INCLUDE file_path_spec ';'
;





// ------------------------------------------------------
// A.1.2 Verilog source text

source_text:
     description*
;

description:
     (attribute_instance* module_keyword) => module_declaration
   |                                         udp_declaration
   |                                         config_declaration
;

module_declaration:
     attribute_instance* module_keyword module_identifier module_parameter_port_list? (  list_of_ports ';'              module_item*         
                                                                                       | list_of_port_declarations? ';' non_port_module_item*
                                                                                      ) K_ENDMODULE
     -> ^(module_keyword module_identifier module_parameter_port_list? list_of_ports? list_of_port_declarations? module_item* non_port_module_item*)
;

module_keyword:
     K_MODULE
   | K_MACROMODULE
;





// ------------------------------------------------------
// A.1.3 Module parameters and ports

module_parameter_port_list:
     '#' '(' parameter_declaration (',' parameter_declaration)* ')' -> parameter_declaration+
;

list_of_ports:
     '(' port (',' port)* ')' -> port+
;

list_of_port_declarations:
     '(' (port_declaration (',' port_declaration)*)? ')' -> port_declaration*
;

port:
     port_expression                                 // TODO: check if optional as in IEEE std
   | '.' port_identifier '(' port_expression? ')'
;

port_expression:
     port_reference
   | '{' port_reference (',' port_reference)* '}'
;

port_reference:
     port_identifier ('[' constant_range_expression ']')?
;

port_declaration:
     attribute_instance* (  inout_declaration
                          | input_declaration
                          | output_declaration
                         )
;


 


// ------------------------------------------------------
// A.1.4 Module items

module_item:
     (port_declaration) =>   port_declaration ';' -> port_declaration
                           | non_port_module_item -> non_port_module_item
;

module_or_generate_item:
     attribute_instance* module_or_generate_item_body
;
module_or_generate_item_body:                    // Extra rule to use 'module_or_generate_item' without 'attribute_instance*'
                            module_or_generate_item_declaration
   |                        local_parameter_declaration ';'
   |                        parameter_override
   |                        continuous_assign
   |                        gate_instantiation
   | (udp_instantiation) => udp_instantiation               // TODO: looks like a special kind of module instantiation
   |                        module_instantiation
   |                        initial_construct
   |                        always_construct
   |                        loop_generate_construct
   |                        conditional_generate_construct
;


module_or_generate_item_declaration:
     net_declaration
   | reg_declaration
   | integer_declaration
   | real_declaration
   | time_declaration
   | realtime_declaration
   | event_declaration
   | genvar_declaration
   | task_declaration
   | function_declaration
;

non_port_module_item:                             // TODO: uncomment
// Uses 'module_or_generate_item_body' instead of 'module_or_generate_item'
// because 'module_or_generate_item' starts with 'attribute_instance*' and
// would need syntactic predicates
     generate_region
//   | specify_block
   | attribute_instance* (  module_or_generate_item_body -> module_or_generate_item_body
                          | parameter_declaration ';'    -> attribute_instance* parameter_declaration   // TODO:
                          | specparam_declaration
                         )
;

parameter_override:
     K_DEFPARAM list_of_defparam_assignments ';'
;





// ------------------------------------------------------
// A.1.5 Configuration source text

config_declaration:
     K_CONFIG config_identifier ';' design_statement config_rule_statement* K_ENDCONFIG
;

design_statement:
     K_DESIGN ((library_identifier '.')? cell_identifier)* ';'
;

config_rule_statement:
     default_clause liblist_clause ';'
   | inst_clause liblist_clause ';'
   | inst_clause use_clause ';'
   | cell_clause liblist_clause ';'
   | cell_clause use_clause ';'
;

default_clause:
     K_DEFAULT
;

inst_clause:
     K_INSTANCE inst_name
;

inst_name:
     topmodule_identifier ('.' instance_identifier)*
;

cell_clause:
     K_CELL (library_identifier ':')? cell_identifier
;

liblist_clause:
     K_LIBLIST library_identifier*
;

use_clause:
     K_USE (library_identifier '.')? cell_identifier (':' K_CONFIG)    // TODO: check if spaces are allowed between ':' and K_CONFIG
;





// --------------------------------------------------------------------------
// A.2 Declarations

// ------------------------------------------------------
// A.2.1 Declaration types

// ----------------------------------
// A.2.1.1 Module parameter declarations

local_parameter_declaration:
//   K_LOCALPARAM K_SIGNED? range? list_of_param_assignments
// | K_LOCALPARAM parameter_type list_of_param_assignments
     K_LOCALPARAM K_SIGNED? range? param_assignment (',' param_assignment)* -> ^(K_LOCALPARAM K_SIGNED? range? param_assignment)+
   | K_LOCALPARAM parameter_type param_assignment (',' param_assignment)*   -> ^(K_LOCALPARAM parameter_type param_assignment)+
;

parameter_declaration:
//   K_PARAMETER K_SIGNED? range? list_of_param_assignments
// | K_PARAMETER parameter_type list_of_param_assignments
     K_PARAMETER K_SIGNED? range? param_assignment (',' param_assignment)* -> ^(K_PARAMETER K_SIGNED? range? param_assignment)+
   | K_PARAMETER parameter_type param_assignment (',' param_assignment)*   -> ^(K_PARAMETER parameter_type param_assignment)+
;

specparam_declaration:
//   K_SPECPARAM range? list_of_specparam_assignments ';'
     K_SPECPARAM range? specparam_assignment (',' specparam_assignment)* ';' -> ^(K_SPECPARAM range? specparam_assignment)+

;

parameter_type:
     K_INTEGER
   | K_REAL
   | K_REALTIME
   | K_TIME
;





// ----------------------------------
// A.2.1.2 Port declarations

inout_declaration:
//   K_INOUT net_type? K_SIGNED? range? list_of_port_identifiers
     K_INOUT net_type? K_SIGNED? range? port_identifier (',' port_identifier)* -> ^(K_INOUT net_type? K_SIGNED? range? port_identifier)+
;

input_declaration:
//   K_INPUT net_type? K_SIGNED? range? list_of_port_identifiers
     K_INPUT net_type? K_SIGNED? range? port_identifier (',' port_identifier)* -> ^(K_INPUT net_type? K_SIGNED? range? port_identifier)+
;

output_declaration:
//   K_OUTPUT net_type? K_SIGNED? range? list_of_port_identifiers
// | K_OUTPUT K_REG K_SIGNED? range? list_of_variable_port_identifiers
// | K_OUTPUT output_variable_type list_of_variable_port_identifiers
     K_OUTPUT net_type? K_SIGNED? range? port_identifier (',' port_identifier)* -> ^(K_OUTPUT net_type? K_SIGNED? range? port_identifier)+
   | K_OUTPUT K_REG K_SIGNED? range? list_of_variable_port_identifiers          -> ^(K_OUTPUT K_REG K_SIGNED? range? list_of_variable_port_identifiers)  // TODO
   | K_OUTPUT output_variable_type list_of_variable_port_identifiers            -> ^(K_OUTPUT output_variable_type list_of_variable_port_identifiers)    // TODO
;





// ----------------------------------
// A.2.1.3 Type declarations

event_declaration:
     K_EVENT list_of_event_identifiers ';'
;

integer_declaration:
//   K_INTEGER list_of_variable_identifiers ';'
     K_INTEGER variable_type (',' variable_type)* ';' -> ^(K_INTEGER variable_type)+
;

net_declaration:
     net_type (
//                                                         K_SIGNED? range? delay3? (  list_of_net_identifiers 
//                                                                                   | list_of_net_decl_assignments) ';'
//             |                (K_VECTORED | K_SCALARED)  K_SIGNED? range  delay3? (  list_of_net_identifiers 
//                                                                                   | list_of_net_decl_assignments) ';'
//             | drive_strength                            K_SIGNED?        delay3? list_of_net_decl_assignments) ';'
//             | drive_strength (K_VECTORED | K_SCALARED)? K_SIGNED? range  delay3? list_of_net_decl_assignments ';' 

                                                           K_SIGNED? range? delay3? (  net_identifier_dimension (',' net_identifier_dimension)*
   -> ^(net_type                                           K_SIGNED? range? delay3?    net_identifier_dimension)+
                                                                                     | net_decl_assignment (',' net_decl_assignment)*
   -> ^(net_type                                           K_SIGNED? range? delay3?    net_decl_assignment)+
                                                                                    ) ';'
               |                 (K_VECTORED | K_SCALARED) K_SIGNED? range  delay3? (  net_identifier_dimension (',' net_identifier_dimension)*
   -> ^(net_type                  K_VECTORED?  K_SCALARED? K_SIGNED? range  delay3?    net_identifier_dimension)+
                                                                                     | net_decl_assignment (',' net_decl_assignment)*
   -> ^(net_type                  K_VECTORED?  K_SCALARED? K_SIGNED? range  delay3?    net_decl_assignment)+
                                                                                    ) ';'
               | drive_strength                            K_SIGNED? range? delay3?    net_decl_assignment (',' net_decl_assignment)* ';'
   -> ^(net_type drive_strength                            K_SIGNED? range? delay3?    net_decl_assignment)+
               | drive_strength  (K_VECTORED | K_SCALARED) K_SIGNED? range  delay3?    net_decl_assignment (',' net_decl_assignment)* ';'
   -> ^(net_type drive_strength   K_VECTORED?  K_SCALARED? K_SIGNED? range  delay3?    net_decl_assignment)+
              ) 

   | K_TRIREG (
//                                                         K_SIGNED? range? delay3? (  list_of_net_identifiers
//                                                                                   | list_of_net_decls_assignments
//                                                                                  ) ';'
//             |                 (K_VECTORED | K_SCALARED) K_SIGNED? range  delay3? (  list_of_net_identifiers
//                                                                                   | list_of_net_decls_assignments
//                                                                                  ) ';'
//             | charge_strength                           K_SIGNED? range? delay3?    list_of_net_identifiers ';'
//             | charge_strength (K_VECTORED | K_SCALARED) K_SIGNED? range  delay3?    list_of_net_identifiers ';'
//             | drive_strength                            K_SIGNED? range? delay3?    list_of_net_decls_assignments ';'
//             | drive_strength  (K_VECTORED | K_SCALARED) K_SIGNED? range  delay3?    list_of_net_decls_assignments ';'
                                                           K_SIGNED? range? delay3? (  net_identifier_dimension (',' net_identifier_dimension)*
   -> ^(K_TRIREG                                           K_SIGNED? range? delay3?    net_identifier_dimension)+
                                                                                     | net_decl_assignment (',' net_decl_assignment)*
   -> ^(K_TRIREG                                           K_SIGNED? range? delay3?    net_decl_assignment)+
                                                                                    ) ';'
               |                 (K_VECTORED | K_SCALARED) K_SIGNED? range  delay3? (  net_identifier_dimension (',' net_identifier_dimension)*
   -> ^(K_TRIREG                  K_VECTORED?  K_SCALARED? K_SIGNED? range  delay3?    net_identifier_dimension)+
                                                                                     | net_decl_assignment (',' net_decl_assignment)*
   -> ^(K_TRIREG                  K_VECTORED?  K_SCALARED? K_SIGNED? range  delay3?    net_decl_assignment)+
                                                                                    ) ';'
               | charge_strength                           K_SIGNED? range? delay3?    net_identifier_dimension (',' net_identifier_dimension)* ';'
   -> ^(K_TRIREG charge_strength                           K_SIGNED? range? delay3?    net_identifier_dimension)+
               | charge_strength (K_VECTORED | K_SCALARED) K_SIGNED? range  delay3?    net_identifier_dimension (',' net_identifier_dimension)* ';'
   -> ^(K_TRIREG charge_strength  K_VECTORED?  K_SCALARED? K_SIGNED? range  delay3?    net_identifier_dimension)+
               | drive_strength                            K_SIGNED? range? delay3?    net_decl_assignment (',' net_decl_assignment)* ';'
   -> ^(K_TRIREG drive_strength                            K_SIGNED? range? delay3?    net_decl_assignment)+
               | drive_strength  (K_VECTORED | K_SCALARED) K_SIGNED? range  delay3?    net_decl_assignment (',' net_decl_assignment)* ';'
   -> ^(K_TRIREG drive_strength   K_VECTORED?  K_SCALARED? K_SIGNED? range  delay3?    net_decl_assignment)+
              )
;
net_identifier_dimension:                         // Extra rule for net identifiers with dimension
     net_identifier dimension* -> net_identifier dimension*    // TODO: Do i need rewrite rule?
;

real_declaration:
//   K_REAL list_of_real_identifiers ';'
     K_REAL real_type (',' real_type)* ';' -> ^(K_REAL real_type)+
;

realtime_declaration:
//   K_REALTIME list_of_real_identifiers ';'
     K_REALTIME real_type (',' real_type)* ';' -> ^(K_REALTIME real_type)+
;

reg_declaration:
//   K_REG K_SIGNED? range? list_of_variable_identifiers ';'
     K_REG K_SIGNED? range? variable_type (',' variable_type)* ';' -> ^(K_REG K_SIGNED? range? variable_type)+
;

time_declaration:
//   K_TIME list_of_variable_identifiers ';'
     K_TIME variable_type (',' variable_type)* ';' -> ^(K_TIME variable_type)+
;





// ------------------------------------------------------
// A.2.2 Declaration data types

// ----------------------------------
// A.2.2.1 Net and variable types

net_type:
     K_SUPPLY0
   | K_SUPPLY1
   | K_TRI
   | K_TRIAND
   | K_TRIOR
   | K_TRI0
   | K_TRI1
   | K_UWIRE
   | K_WIRE
   | K_WAND
   | K_WOR
;

output_variable_type:
     K_INTEGER
   | K_TIME
;

real_type:
     real_identifier dimension*
   | real_identifier '=' constant_expression
;

variable_type:
     variable_identifier dimension*              -> variable_identifier dimension*
   | variable_identifier '=' constant_expression -> variable_identifier constant_expression
;





// ----------------------------------
// A.2.2.2 Strengths

drive_strength:
     '(' strength0 ',' strength1 ')'
   | '(' strength1 ',' strength0 ')'
   | '(' strength0 ',' K_HIGHZ1  ')'
   | '(' strength1 ',' K_HIGHZ0  ')'
   | '(' K_HIGHZ0  ';' strength1 ')'
   | '(' K_HIGHZ1  ';' strength0 ')'
;

strength0:
     K_SUPPLY0
   | K_STRONG0
   | K_PULL0
   | K_WEAK0
;

strength1:
     K_SUPPLY1
   | K_STRONG1
   | K_PULL1
   | K_WEAK1
;

charge_strength:
     '(' K_SMALL  ')'
   | '(' K_MEDIUM ')'
   | '(' K_LARGE  ')'
;





// ----------------------------------
// A.2.2.3 Delays

delay3:
     '#' delay_value
   | '#' '(' mintypmax_expression (  ')'
                                   | ',' mintypmax_expression (  ')'
                                                               | ',' mintypmax_expression ')'
                                                              )
                                  )
;

delay2:
     '#' delay_value
   | '#' '(' mintypmax_expression (  ')'
                                   | ',' mintypmax_expression ')'
                                  )
;

delay_value:
     UNSIGNED_NUMBER
   | real_number
   | IDENTIFIER
;





// ------------------------------------------------------
// A.2.3 Declaration lists

list_of_defparam_assignments:
     defparam_assignment (',' defparam_assignment)*
;

list_of_event_identifiers:
     event_identifier dimension* (',' event_identifier dimension*)*
;

//list_of_net_decl_assignments:
//     net_decl_assignment (',' net_decl_assignment)*
//;
     
//list_of_net_identifiers:
//     net_identifier dimension* (',' net_identifier dimension*)*
//;

//list_of_param_assignments:
//     param_assignment (',' param_assignment)*
//;

//list_of_port_identifiers:
//     port_identifier (',' port_identifier)*
//;

//list_of_real_identifiers:
//     real_type (',' real_type)*
//;

//list_of_specparam_assignments:
//     specparam_assignment (',' specparam_assignment)*
//;

//list_of_variable_identifiers:                   // Moved to integer reg and time declarations
//     variable_type (',' variable_type)*
//;

list_of_variable_port_identifiers:
     list_of_variable_port_identifiers_body (',' list_of_variable_port_identifiers_body)*
;
list_of_variable_port_identifiers_body:           // Needed to distinguish between IDENTIFIER and IDENTIFIER = constant_expression
/*     port_identifier
   |*/
     port_identifier ('=' constant_expression)?
;   // TODO: check if body rule can be included in main rule





// ------------------------------------------------------
// A.2.4 Declaration assignments

defparam_assignment:
     hierarchical_parameter_identifier '=' constant_mintypmax_expression
;

net_decl_assignment:
     net_identifier '=' expression -> net_identifier expression
;

param_assignment:
     parameter_identifier '=' constant_mintypmax_expression -> parameter_identifier constant_mintypmax_expression
;

specparam_assignment:
     specparam_identifier '=' constant_mintypmax_expression
   | pulse_control_specparam
;

pulse_control_specparam:    // TODO: what the fuck is this and are the spaces around the '$'s okay? 
     'PATHPULSE$' '=' '(' reject_limit_value (',' error_limit_value)? ')'
   | 'PATHPULSE$' specify_input_terminal_descriptor '$' specify_output_terminal_descriptor '=' '(' reject_limit_value (',' error_limit_value)? ')'
;

error_limit_value:
     limit_value
;

reject_limit_value:
     limit_value
;

limit_value:
     constant_mintypmax_expression
;





// ------------------------------------------------------
// A.2.5 Declaration ranges

dimension:                         // TODO: check if RANGE label is OK or if DIMENSION label is better
   '[' constant_expression ':' constant_expression ']' -> ^(RANGE ':' constant_expression*)
;

range:
   '[' constant_expression ':' constant_expression ']' -> ^(RANGE ':' constant_expression*)
;





// ------------------------------------------------------
// A.2.6 Function declarations

function_declaration:
     K_FUNCTION K_AUTOMATIC? function_range_or_type function_identifier (  ';' ((function_item_declaration) => function_item_declaration)+ function_statement
                                                                         | '(' function_port_list ')' ';' ((block_item_declaration) => block_item_declaration)* function_statement
                                                                         ) K_ENDFUNCTION
;

function_item_declaration:
     attribute_instance* (  block_item_declaration_body
                          | tf_input_declaration ';'
                          )
;

function_port_list:
     attribute_instance* tf_input_declaration (',' attribute_instance* tf_input_declaration)*
;

function_range_or_type:
     K_SIGNED? range?
   | K_INTEGER
   | K_REAL
   | K_REALTIME
   | K_TIME
;





// ------------------------------------------------------
// A.2.7 Task declarations

task_declaration:
     K_TASK K_AUTOMATIC? task_identifier (  ';' ((task_item_declaration) => task_item_declaration)* statement_or_null
                                          | '(' task_port_list ')' ';' ((block_item_declaration) => block_item_declaration)* statement_or_null
                                          ) K_ENDTASK
;

task_item_declaration:
// Uses 'block_item_declaration_body' instead of 'block_item_declaration'.
// 'block_item_declaration' would start with 'attribute_instance*' and therefore
// syntactic predicates would be needed.
     attribute_instance* (  block_item_declaration_body
                          | tf_input_declaration ';'
                          | tf_output_declaration ';'
                          | tf_inout_declaration ';'
                         )
;

task_port_list:
     task_port_item (',' task_port_item)*
;

task_port_item:
     attribute_instance* (  tf_input_declaration
                          | tf_output_declaration
                          | tf_inout_declaration
                         )
;

tf_input_declaration:
//   K_INPUT K_REG? K_SIGNED? range? list_of_port_identifiers
// | K_INPUT task_port_type list_of_port_identifiers
     K_INPUT K_REG? K_SIGNED? range? port_identifier (',' port_identifier)*
   | K_INPUT task_port_type port_identifier (',' port_identifier)*
;

tf_output_declaration:
//   K_OUTPUT K_REG? K_SIGNED? range? list_of_port_identifiers
// | K_OUTPUT task_port_type list_of_port_identifiers
     K_OUTPUT K_REG? K_SIGNED? range? port_identifier (',' port_identifier)*
   | K_OUTPUT task_port_type port_identifier (',' port_identifier)*
;

tf_inout_declaration:
//   K_INOUT K_REG? K_SIGNED? range? list_of_port_identifiers
// | K_INOUT task_port_type list_of_port_identifiers
     K_INOUT K_REG? K_SIGNED? range? port_identifier (',' port_identifier)*
   | K_INOUT task_port_type port_identifier (',' port_identifier)*
;

task_port_type:
     K_INTEGER
   | K_REAL
   | K_REALTIME
   | K_TIME
;





// ------------------------------------------------------
// A.2.8 Block item declarations

block_item_declaration:
     attribute_instance* block_item_declaration_body
;
block_item_declaration_body:                     
// Extra rule to use 'block_item_declaration' without 'attribute_instance*'
     K_REG K_SIGNED? range? list_of_block_variable_identifiers ';'
   | K_INTEGER list_of_block_variable_identifiers ';'
   | K_TIME list_of_block_variable_identifiers ';'
   | K_REAL list_of_block_real_identifiers ';'
   | K_REALTIME list_of_block_real_identifiers ';'
   | event_declaration
   | local_parameter_declaration ';'
   | parameter_declaration ';'
;

list_of_block_variable_identifiers:
     block_variable_type (',' block_variable_type)*
;

list_of_block_real_identifiers:
     block_real_type (',' block_real_type)*
;

block_variable_type:
     variable_identifier dimension*
;

block_real_type:
     real_identifier dimension*
;





// --------------------------------------------------------------------------
// A.3 Primitive instances

// ------------------------------------------------------
// A.3.1 Primitive instantiation and instances

gate_instantiation:
     cmos_switchtype                       delay3? cmos_switch_instance (',' cmos_switch_instance)* ';'
   | enable_gatetype    drive_strength?    delay3? enable_gate_instance (',' enable_gate_instance)* ';'
   | mos_switchtype                        delay3? mos_switch_instance (',' mos_switch_instance)* ';'
   | n_input_gatetype   drive_strength?    delay2? n_input_gate_instance (',' n_input_gate_instance)* ';'
   | n_output_gatetype  drive_strength?    delay2? n_output_gate_instance (',' n_output_gate_instance)* ';'
   | pass_en_switchtype                    delay2? pass_enable_switch_instance (',' pass_enable_switch_instance)* ';'
   | pass_switchtype                               pass_switch_instance (',' pass_switch_instance)* ';'
   | K_PULLDOWN         pulldown_strength?         pull_gate_instance (',' pull_gate_instance)* ';'
   | K_PULLUP           pullup_strength?           pull_gate_instance (',' pull_gate_instance)* ';'
;

cmos_switch_instance:
     name_of_gate_instance? '(' output_terminal ',' input_terminal ',' ncontrol_terminal ',' pcontrol_terminal ')'
;

enable_gate_instance:
     name_of_gate_instance? '(' output_terminal ',' input_terminal ',' enable_terminal ')'
;

mos_switch_instance:
     name_of_gate_instance? '(' output_terminal ',' input_terminal ',' enable_terminal ')'
;

n_input_gate_instance:
     name_of_gate_instance? '(' output_terminal ',' input_terminal (',' input_terminal)* ')'
;

n_output_gate_instance:
     name_of_gate_instance? '(' output_terminal ',' ((output_terminal ',') => output_terminal ',')* input_terminal ')'
;

pass_switch_instance:
     name_of_gate_instance? '(' inout_terminal ',' inout_terminal ')'
;

pass_enable_switch_instance:
     name_of_gate_instance? '(' inout_terminal ',' inout_terminal ',' enable_terminal ')'
;

pull_gate_instance:
     name_of_gate_instance? '(' output_terminal ')'
;


name_of_gate_instance:
     gate_instance_identifier range?
;





// ------------------------------------------------------
// A.3.2 Primitive strengths

pulldown_strength:
     '(' strength0 ',' strength1 ')'
   | '(' strength1 ',' strength0 ')'
   | '(' strength0 ')'
;

pullup_strength:
     '(' strength0 ',' strength1 ')'
   | '(' strength1 ',' strength0 ')'
   | '(' strength1 ')'
;





// ------------------------------------------------------
// A.3.3 Primitive terminals

enable_terminal:
     expression
;

inout_terminal:
     net_lvalue
;

input_terminal:
     expression
;

ncontrol_terminal:
     expression
;

output_terminal:
     net_lvalue
;

pcontrol_terminal:
     expression
;





// ------------------------------------------------------
// A.3.4 Primitive gate and switch types

cmos_switchtype:
     K_CMOS
   | K_RCMOS
;

enable_gatetype:
     K_BUFIF0
   | K_BUFIF1
   | K_NOTIF0
   | K_NOTIF1
;

mos_switchtype:
     K_NMOS
   | K_PMOS
   | K_RNMOS
   | K_RPMOS
;

n_input_gatetype:
     K_AND
   | K_NAND
   | K_OR
   | K_NOR
   | K_XOR
   | K_XNOR
;

n_output_gatetype:
     K_BUF
   | K_NOT
;

pass_en_switchtype:
     K_TRANIF0
   | K_TRANIF1
   | K_RTRANIF1
   | K_RTRANIF0
;

pass_switchtype:
     K_TRAN
   | K_RTRAN     
;





// --------------------------------------------------------------------------
// A.4 Module instantiation and generate construct

// ------------------------------------------------------
// A.4.1 Module instantiation

module_instantiation:
     module_identifier parameter_value_assignment? module_instance (',' module_instance)* ';' -> ^(MODULE_INSTANCE module_identifier module_instance parameter_value_assignment?)+
;

parameter_value_assignment:
     '#' '(' list_of_parameter_assignments ')' -> ^(PARAMETER_ASSIGNMENTS list_of_parameter_assignments)
;

list_of_parameter_assignments:
     ordered_parameter_assignment (',' ordered_parameter_assignment)* -> ordered_parameter_assignment+
   | named_parameter_assignment (',' named_parameter_assignment)*     -> named_parameter_assignment+
;

ordered_parameter_assignment:
     expression -> ^(ORDERED expression)
;

named_parameter_assignment:
     '.' parameter_identifier '(' mintypmax_expression? ')' -> ^(NAMED parameter_identifier mintypmax_expression?)
;

module_instance:    // [list_of_port_connections] in IEEE std; this is useless, as 'list_of_port_connections->ordered_port_connection' may be empty as well
     name_of_module_instance '(' list_of_port_connections ')' -> name_of_module_instance ^(PORT_CONNECTIONS list_of_port_connections)
;

name_of_module_instance:   // TODO: range
     module_instance_identifier range? -> module_instance_identifier
;

list_of_port_connections:
     (ordered_port_connection) => ordered_port_connection (',' ordered_port_connection)* -> ordered_port_connection+
   |                              named_port_connection (',' named_port_connection)*     -> named_port_connection+
;

ordered_port_connection:   // TODO: attribute instance
     attribute_instance* expression? -> ^(ORDERED expression?)
;

named_port_connection:   // TODO: attribute instance
     attribute_instance* '.' port_identifier '(' expression? ')' -> ^(NAMED port_identifier expression?)
;





// ------------------------------------------------------
// A.4.2 Generate construct

generate_region:
     K_GENERATE (module_or_generate_item)* K_ENDGENERATE
;

genvar_declaration:
     K_GENVAR list_of_genvar_identifiers ';'
;

list_of_genvar_identifiers:
     genvar_identifier (',' genvar_identifier)*
;

loop_generate_construct:
     K_FOR '(' genvar_initialization ';' genvar_expression ';' genvar_iteration ')' generate_block
;

genvar_initialization:
     genvar_identifier '=' constant_expression
;

genvar_expression:
// Can't use @init/@after here because @after is not evaluated during backtracking but @init is. 
     { generate_expression_cnt++; }
     expression { generate_expression_cnt--; }
;

genvar_iteration:
     genvar_identifier '=' genvar_expression
;

genvar_primary:
     constant_primary
//   | genvar_identifier     // TODO: this is the same as e.g. parameter_identifier in constant_primary
;

conditional_generate_construct:
     if_generate_construct
   | case_generate_construct
;

if_generate_construct:     // TODO: syntactic predicate sucks and forces many backtracking steps
     (K_IF '(' constant_expression ')' generate_block_or_null K_ELSE) => K_IF '(' constant_expression ')' generate_block_or_null K_ELSE generate_block_or_null
   |                                                                     K_IF '(' constant_expression ')' generate_block_or_null
;

case_generate_construct:
     K_CASE '('constant_expression ')' case_generate_item+ K_ENDCASE
;

case_generate_item:
     constant_expression (',' constant_expression)* ':' generate_block_or_null
   | K_DEFAULT ':'? generate_block_or_null
;

generate_block:
     module_or_generate_item
   | K_BEGIN (':' generate_block_identifier)? module_or_generate_item* K_END
;

generate_block_or_null:
     generate_block
   | ';'
;





// --------------------------------------------------------------------------
// A.5 UDP declaration and instantiation

// ------------------------------------------------------
// A.5.1 UDP declaration

udp_declaration:
     attribute_instance* K_PRIMITIVE udp_identifier '(' (  udp_port_list ')' ';' udp_port_declaration+ 
                                                         | udp_declaration_port_list ')' ';'
                                                        ) udp_body K_ENDPRIMITIVE
;





// ------------------------------------------------------
// A.5.2 UDP ports

udp_port_list:
     output_port_identifier ',' input_port_identifier (',' input_port_identifier)*
;

udp_declaration_port_list:
     udp_output_declaration ',' udp_input_declaration (',' udp_input_declaration)*
;

udp_port_declaration:     // TODO: do i need these syntactic predicates
     (udp_output_declaration) => udp_output_declaration ';'
   | (udp_input_declaration)  => udp_input_declaration ';'
   |                             udp_reg_declaration ';'
;

udp_output_declaration:
     attribute_instance* K_OUTPUT (        port_identifier
                                   | K_REG port_identifier ('=' constant_expression)?
                                  )
;

udp_input_declaration:
//   attribute_instance* K_INPUT list_of_port_identifiers
     attribute_instance* K_INPUT port_identifier (',' port_identifier)*
;

udp_reg_declaration:
     attribute_instance* K_REG variable_identifier
;





// ------------------------------------------------------
// A.5.3 UDP body

udp_body:
     combinational_body
   | sequential_body
;

combinational_body:
     K_TABLE combinational_entry+ K_ENDTABLE
;

combinational_entry:
     level_input_list ':' output_symbol ';'
;

sequential_body:
     udp_initial_statement? K_TABLE sequential_entry+ K_ENDTABLE
;

udp_initial_statement:
     K_INITIAL output_port_identifier '=' init_val ';'
;

init_val:         // TODO: How can i do this?
     UNSIGNED_NUMBER BINARY_VALUE
   | UNSIGNED_NUMBER
//   '1\'b0'
// | '1\'b1'
// | '1\'bx'
// | '1\'bX'
// | '1\'B0'
// | '1\'B1'
// | '1\'Bx'
// | '1\'BX'
// | '1'
// | '0'
;

sequential_entry:
     seq_input_list ':' current_state ':' next_state ';'
;

seq_input_list:
     (edge_input_list) => edge_input_list
   |                      level_input_list
;

level_input_list:
     level_symbol+
;

edge_input_list:
     level_symbol* edge_indicator level_symbol*
;

edge_indicator:
     '(' level_symbol level_symbol ')'
   | edge_symbol
;

current_state:
     level_symbol
;

next_state:
     output_symbol
   | '-'
;

output_symbol:                // TODO: How can i do this?
     UNSIGNED_NUMBER
   | IDENTIFIER
//   '0'
// | '1'
// | 'x'
// | 'X'
;

level_symbol:                 // TODO: How can i do this?
//   '0'
// | '1'
// | 'x'
// | 'X'
// | '?'
// | 'b'
// | 'B'
     '?'
;

edge_symbol:           // TODO: How can i do this?
//   'r'
// | 'R'
// | 'f'
// | 'F'
// | 'p'
// | 'P'
// | 'n'
// | 'N'
// | '*'
     '*'
;





// ------------------------------------------------------
// A.5.4 UDP instantiation

udp_instantiation:
     udp_identifier drive_strength? delay2? udp_instance (',' udp_instance)* ';'
;

udp_instance:
     name_of_udp_instance? '(' output_terminal ',' input_terminal (',' input_terminal)* ')'
;

name_of_udp_instance:
     udp_instance_identifier range?
;





// --------------------------------------------------------------------------
// A.6 Behavioral statements

// ------------------------------------------------------
// A.6.1 Continuous assignment statements

continuous_assign:
     K_ASSIGN drive_strength? delay3? list_of_net_assignments ';'
;

list_of_net_assignments:
     net_assignment (',' net_assignment)*
;

net_assignment: 
     net_lvalue '=' expression
;





// ------------------------------------------------------
// A.6.2 Procedural blocks and assignments

initial_construct:
     K_INITIAL statement
;

always_construct:
     K_ALWAYS statement
;

blocking_assignment:
     variable_lvalue '=' (delay_or_event_control)? expression
;

nonblocking_assignment:
     variable_lvalue '<=' (delay_or_event_control)? expression
;

procedural_continuous_assignments:
     K_ASSIGN variable_assignment
   | K_DEASSIGN variable_lvalue
   | K_FORCE variable_assignment
//   | K_FORCE net_assignment    // TODO: same as above
   | K_RELEASE variable_lvalue
//   | K_RELEASE net_lvalue      // TODO: same as above
;

variable_assignment:
     variable_lvalue '=' expression
;





// ------------------------------------------------------
// A.6.3 Parallel and sequential blocks

par_block:
     K_FORK                                                                            statement* K_JOIN
   | K_FORK ':' block_identifier ((block_item_declaration) => block_item_declaration)* statement* K_JOIN
;

seq_block:
     K_BEGIN                                                                            statement* K_END
   | K_BEGIN ':' block_identifier ((block_item_declaration) => block_item_declaration)* statement* K_END
;





// ------------------------------------------------------
// A.6.4 Statements

statement:
     attribute_instance* statement_body
;
statement_body:                                  // TODO: reduce syntactic predicates if possible
// Extra rule to use 'statement' without 'attribute_instance*'
     (blocking_assignment) => blocking_assignment ';'
   | case_statement
   | conditional_statement
   | disable_statement
   | event_trigger
   | loop_statement
   | (nonblocking_assignment) => nonblocking_assignment ';'
   | par_block
   | procedural_continuous_assignments ';'
   | procedural_timing_control_statement
   | seq_block
   | system_task_enable
   | task_enable
   | wait_statement
;


statement_or_null:
// Uses 'statement_body' instead of 'statement' because use of 'statement' 
// would need syntactic predicates because it start with 'attribute_instance*'
     attribute_instance* (  statement_body
                          | ';'
                         )
;

function_statement: 
     statement   // TODO: mark as function statement
;





// ------------------------------------------------------
// A.6.5 Timing control statements

delay_control:
     '#' delay_value
   | '#' '(' mintypmax_expression ')'
;

delay_or_event_control:
     delay_control
   | event_control
   | K_REPEAT '(' expression ')' event_control
;

disable_statement:   
     K_DISABLE hierarchical_task_identifier ';'
//   | K_DISABLE hierarchical_block_identifier ';'  // TODO: same as above
;

event_control:
     '@' hierarchical_event_identifier 
   | '@' '(' event_expression_many ')'   // TODO: get rid of ..._many rule
   | '@' '*'
   | '@' '(' '*' ')'
;

event_trigger:
     '->' hierarchical_event_identifier ('[' expression ']')* ';'
;

event_expression:  // TODO: get rid of ..._many rule
//   (K_POSEDGE | K_NEGEDGE)? expression
     expression
   | K_POSEDGE expression
   | K_NEGEDGE expression
;
event_expression_many  // TODO: get rid of ..._many rule
options {
   backtrack=true;
}:   // TODO: check if 'or' and ','  may be used together
     event_expression
   | event_expression (K_OR event_expression)*
   | event_expression (',' event_expression)*
;

procedural_timing_control:
     delay_control
   | event_control
;

procedural_timing_control_statement:
     procedural_timing_control statement_or_null
;

wait_statement:
     K_WAIT '(' expression ')' statement_or_null
;





// ------------------------------------------------------
// A.6.6 Conditional statements

conditional_statement:   // TODO: syntactic predicate sucks and forces many backtracking steps
    (K_IF '(' expression ')' statement_or_null K_ELSE) => K_IF '(' expression ')' statement_or_null K_ELSE statement_or_null -> ^(K_IF expression statement_or_null+)
  |                                                       K_IF '(' expression ')' statement_or_null                          -> ^(K_IF expression statement_or_null)
;





// ------------------------------------------------------
// A.6.7 Case statements

case_statement:
     case_keyword '(' expression ')' case_item+ K_ENDCASE
;

case_item:
     expression (',' expression)* ':' statement_or_null
   | K_DEFAULT ':'? statement_or_null
;

case_keyword:
     K_CASE
   | K_CASEZ
   | K_CASEX
;





// ------------------------------------------------------
// A.6.8 Looping statements

loop_statement:
     K_FOREVER statement
   | K_REPEAT '(' expression ')' statement
   | K_WHILE '(' expression ')' statement
   | K_FOR '(' variable_assignment ';' expression ';' variable_assignment ')' statement
;





// ------------------------------------------------------
// A.6.9 Task enable statements

system_task_enable:
     system_task_identifier ('(' expression? (',' expression?)* ')')? ';'
;

task_enable:
     hierarchical_task_identifier ('(' expression (',' expression)* ')')? ';'
;





// --------------------------------------------------------------------------
// A.7 Specify section

// ------------------------------------------------------
// A.7.3 Specify block terminals

specify_input_terminal_descriptor:
     input_identifier ('[' constant_range_expression ']')?
;

specify_output_terminal_descriptor:
     output_identifier ('[' constant_range_expression ']')?
;

input_identifier:
     input_port_identifier
//   | inout_port_identifier   // TODO: same as above
;

output_identifier:
     output_port_identifier
//   | inout_port_identifier   // TODO: same as above
;





// --------------------------------------------------------------------------
// A.8 Expressions

// ------------------------------------------------------
// A.8.1 Concatenations

concatenation:
     '{' expression (',' expression)* '}' -> ^(CONCAT expression+)
;

constant_concatenation:
     '{' constant_expression (',' constant_expression)* '}' -> ^(CONCAT constant_expression+)
;

constant_multiple_concatenation:
     '{' constant_expression constant_concatenation '}' -> ^(MULT_CONCAT constant_expression constant_concatenation)
;

multiple_concatenation:
     '{' constant_expression concatenation '}' -> ^(MULT_CONCAT constant_expression concatenation)
;





// ------------------------------------------------------
// A.8.2 Function calls

constant_function_call:
     function_identifier attribute_instance* '(' constant_expression (',' constant_expression)* ')'
;
	
constant_system_function_call:
     system_function_identifier '(' constant_expression (',' constant_expression)* ')'
;

function_call:
     hierarchical_function_identifier attribute_instance* '(' expression (',' expression)* ')'
;

system_function_call:
     system_function_identifier ('(' expression (',' expression)* ')')?
;





// ------------------------------------------------------
// A.8.3 Expressions

conditional_expression:                          // TODO: check operator precedence
     (log_or_expression -> log_or_expression)                              
     ('?' attribute_instance* conditional_expression      
      ':' conditional_expression
      -> ^('?' log_or_expression conditional_expression*)
     )?
;

constant_expression:
// Can't use @init/@after here because @after is not evaluated during backtracking but @init is. 
     { constant_expression_cnt++; }
     expression { constant_expression_cnt--; }
     -> ^(CONST expression)
;

constant_mintypmax_expression:
     constant_expression (':' constant_expression ':' constant_expression)?
;

constant_range_expression:
     (ce1=constant_expression        -> ^(RANGE $ce1))
     (  ':'  ce2=constant_expression -> ^(RANGE ':' $ce1 $ce2)
      | '+:' ce2=constant_expression -> ^(RANGE '+:' $ce1 $ce2)
      | '-:' ce2=constant_expression -> ^(RANGE '-:' $ce1 $ce2)
     )?
;

unary_expression:
     primary                                    -> primary   
   | unary_operator attribute_instance* primary -> ^(unary_operator primary attribute_instance*);		

pow_expression:
     (unary_expression -> unary_expression)                             
     ('**' attribute_instance* pe=pow_expression
      -> ^('**' unary_expression $pe attribute_instance*)  
     )?
;

mult_div_mod_expression:
     (pow_expression -> pow_expression)                                                  
     (binary_mult_div_operator attribute_instance* pow_expression
      -> ^(binary_mult_div_operator $mult_div_mod_expression pow_expression attribute_instance*)
     )*
;

plus_minus_expression:
     (mult_div_mod_expression -> mult_div_mod_expression)                                                    
     (binary_plus_minus_operator attribute_instance* mult_div_mod_expression 
      -> ^(binary_plus_minus_operator $plus_minus_expression mult_div_mod_expression attribute_instance*)
     )*
;

shift_expression:
     (plus_minus_expression -> plus_minus_expression)                                                
     (binary_shift_operator attribute_instance* plus_minus_expression
      -> ^(binary_shift_operator $shift_expression plus_minus_expression attribute_instance*)
     )*
;

less_greater_expression:
     (shift_expression -> shift_expression)                                                       
     (binary_less_greater_operator attribute_instance* shift_expression     
      -> ^(binary_less_greater_operator $less_greater_expression shift_expression attribute_instance*)
     )*
;

eq_neq_expression:
     (less_greater_expression -> less_greater_expression)                                                
     (binary_eq_neq_operator attribute_instance* less_greater_expression    
      -> ^(binary_eq_neq_operator $eq_neq_expression less_greater_expression attribute_instance*)
     )*
;

and_expression:
     (eq_neq_expression -> eq_neq_expression)                           
     ('&' attribute_instance* eq_neq_expression 
      -> ^('&' $and_expression eq_neq_expression attribute_instance*)
     )*
;

xor_expression:
     (and_expression -> and_expression)                                             
     (binary_xor_operator attribute_instance* and_expression 
      -> ^(binary_xor_operator $xor_expression and_expression attribute_instance*)
     )*
;

or_expression:
     (xor_expression -> xor_expression)                                                 
     ('|' attribute_instance* xor_expression  
      -> ^('|' $or_expression xor_expression attribute_instance*)
     )*
;

log_and_expression:
     (or_expression -> or_expression)                                                   
     ('&&' attribute_instance* or_expression  
      -> ^('&&' $log_and_expression or_expression attribute_instance*)
     )*
;

log_or_expression:
     (log_and_expression -> log_and_expression)                                                  
     ('||' attribute_instance* log_and_expression  
      -> ^('||' $log_or_expression log_and_expression attribute_instance*)
     )*
;

expression:
   conditional_expression
;

mintypmax_expression:
     expression (':' expression ':' expression)?
;

range_expression:
     (constant_expression ':') => constant_expression ':' constant_expression -> ^(RANGE ':' constant_expression*)
   |                              expression (                                -> ^(RANGE expression)
                                              | '+:' constant_expression      -> ^(RANGE '+:' expression constant_expression)
                                              | '-:' constant_expression      -> ^(RANGE '-:' expression constant_expression)
                                             )
;





// ------------------------------------------------------
// A.8.4 Primaries

constant_primary:
// The first two entries are identical to non-constant primary
//   number
// | string
// The following rules are unique to constant primary
     (constant_function_call) => constant_function_call
   |                             parameter_identifier ('[' constant_range_expression ']')?
//   |                             specparam_identifier ('[' constant_range_expression ']')?     // TODO: Same as above
   | (constant_concatenation) => constant_concatenation
   |                             constant_multiple_concatenation
   |                             constant_system_function_call
   |                             '(' constant_mintypmax_expression ')'
;



primary:                                      // TODO: uncomment string
                        number                                                                      
   |                    STRING
   // The following line is used for constant expressions
   | {constant_expression_cnt != 0}? constant_primary
   // The following line is used for genvar expressions
   | {generate_expression_cnt != 0}? genvar_primary
   // The rest is used for non-constant expressions
   | (function_call) => function_call
   |                    hierarchical_identifier ('[' ((expression ']' '[') => (expression ']' '['))* range_expression ']')?
                        -> ^(HIERARCHICAL_ID hierarchical_identifier ^(RANGE expression)* range_expression?)
   | (concatenation) => concatenation
   |                    multiple_concatenation
   |                    system_function_call
   |                    '(' mintypmax_expression ')' -> mintypmax_expression
;





// ------------------------------------------------------
// A.8.5 Expression left-side values
net_lvalue:
     hierarchical_net_identifier ( '[' ((constant_expression ']' '[') => (constant_expression ']' '['))* constant_range_expression ']')?
   | '{' net_lvalue (',' net_lvalue)* '}'
;

variable_lvalue:
     hierarchical_variable_identifier ( '[' ((expression ']' '[') => (expression ']' '['))* range_expression ']')?
   | '{' variable_lvalue (',' variable_lvalue)* '}'
;





// ------------------------------------------------------
// A.8.6 Operators

unary_operator:      
     '+'  
   | '-'  
   | '!'  
   | '~'  
   | '&'  
   | '~&' 
   | '|'  
   | '~|' 
   | '^'  
   | '~^' 
   | '^~'
;

binary_mult_div_operator:
     '*' 
   | '/' 
   | '%' 
;

binary_plus_minus_operator:
     '+'  
   | '-' 
;

binary_shift_operator:
     '<<'  
   | '>>'  
   | '<<<' 
   | '>>>' ;

binary_less_greater_operator:
     '<'  
   | '<=' 
   | '>'  
   | '>=' 
;

binary_eq_neq_operator:
     '=='  
   | '!='  
   | '===' 
   | '!==' 
;

binary_xor_operator:
     '^'  
   | '~^' 
   | '^~' 
;





// ------------------------------------------------------
// A.8.7 Numbers

number:
     (binary_number)  => binary_number   
   | (hex_number)     => hex_number      
   | (decimal_number) => decimal_number  
   | (octal_number)   => octal_number    
   | (real_number)    => real_number     
   |                     UNSIGNED_NUMBER 
;

real_number:
     REAL_VALUE
;

decimal_number:
     UNSIGNED_NUMBER DECIMAL_VALUE   // TODO: does not check for non-zero number in bit size
   |                 DECIMAL_VALUE
;

binary_number:
     UNSIGNED_NUMBER BINARY_VALUE    // TODO: does not check for non-zero number in bit size
   |                 BINARY_VALUE 
;

octal_number:
     UNSIGNED_NUMBER OCTAL_VALUE     // TODO: does not check for non-zero number in bit size
   |                 OCTAL_VALUE 
;

hex_number:
     UNSIGNED_NUMBER HEX_VALUE       // TODO: does not check for non-zero number in bit size
   |                 HEX_VALUE 
;

UNSIGNED_NUMBER:
     DECIMAL_DIGIT ('_' | DECIMAL_DIGIT)*
;

REAL_VALUE:
     UNSIGNED_NUMBER ('.' UNSIGNED_NUMBER)? ('e' | 'E') ('+' | '-')? UNSIGNED_NUMBER
   | UNSIGNED_NUMBER  '.' UNSIGNED_NUMBER
;

DECIMAL_VALUE:
     DECIMAL_BASE WS? (  UNSIGNED_NUMBER
                       | X_DIGIT '_'*
                       | Z_DIGIT '_'*
                      )
;

BINARY_VALUE:
     BINARY_BASE WS? BINARY_DIGIT ('_' | BINARY_DIGIT)*
;

OCTAL_VALUE:
     OCTAL_BASE WS? OCTAL_DIGIT ('_' | OCTAL_DIGIT)*
;

HEX_VALUE:
     HEX_BASE WS? HEX_DIGIT ('_' | HEX_DIGIT)*
;

fragment
DECIMAL_BASE:
     '\'' ('s' | 'S')? ('d' | 'D') 
;

fragment
BINARY_BASE:
     '\'' ('s' | 'S')? ('b' | 'B') 
;

fragment
OCTAL_BASE:
     '\'' ('s' | 'S')? ('o' | 'O') 
;

fragment
HEX_BASE:
     '\'' ('s' | 'S')? ('h' | 'H') 
;

//fragment
//NON_ZERO_DECIMAL_DIGIT:
//     '1'..'9'
//;

fragment
DECIMAL_DIGIT:
     '0'..'9'
;

fragment
BINARY_DIGIT:
     X_DIGIT
   | Z_DIGIT
   | '0'..'1'
;

fragment
OCTAL_DIGIT:
     X_DIGIT
   | Z_DIGIT
   | '0'..'7'
;

fragment
HEX_DIGIT:
     X_DIGIT
   | Z_DIGIT
   | '0'..'9'
   | 'a'..'f'
   | 'A'..'F'
;

fragment
X_DIGIT:
     'x'
   | 'X'
;

fragment
Z_DIGIT:
     'z'
   | 'Z'
   | '?'
;





// ------------------------------------------------------
// A.8.8 Strings
STRING:
     '"' (options{ greedy=false; }: ~'\n')* '"'
;





// --------------------------------------------------------------------------
// A.9 General

// ------------------------------------------------------
// A.9.1 Attributes

attribute_instance:
     '(*' attr_spec  (',' attr_spec  )* '*)' -> attr_spec+
;

attr_spec:
     attr_name ('=' constant_expression)? -> ^(ATTRIBUTE attr_name constant_expression?)
;

attr_name:
     IDENTIFIER  
;





// ------------------------------------------------------
// A.9.2 Comments

COMMENT:
     '//' ~('\n' | '\r')* '\r'? '\n'          { $channel=HIDDEN; }
   | '/*' (options{ greedy=false; }: .)* '*/' { $channel=HIDDEN; }
;





// ------------------------------------------------------
// A.9.3 Identifiers

block_identifier:
     IDENTIFIER
;

cell_identifier:
     IDENTIFIER
;

config_identifier:
     IDENTIFIER
;

fragment
ESCAPED_IDENTIFIER: // TODO: is the * ok or should it be +
     '\\' (options{ greedy=false; }: .)* WS
;

event_identifier:
     IDENTIFIER
;

function_identifier:
     IDENTIFIER
;

gate_instance_identifier:
     IDENTIFIER
;

generate_block_identifier:
     IDENTIFIER
;

genvar_identifier:
     IDENTIFIER
;

hierarchical_block_identifier:
     hierarchical_identifier
;

hierarchical_event_identifier:
     hierarchical_identifier
;

hierarchical_function_identifier: 
     hierarchical_identifier
;

hierarchical_identifier: 
     ((hierarchical_identifier_path) => hierarchical_identifier_path)* IDENTIFIER
;
hierarchical_identifier_path:                          // Extra rule for the hierarchical path of hierarchical identifiers
     IDENTIFIER ('[' constant_expression ']')? '.' -> IDENTIFIER ^(RANGE constant_expression)?
;

hierarchical_net_identifier: 
     hierarchical_identifier
;

hierarchical_parameter_identifier: 
     hierarchical_identifier
;

hierarchical_variable_identifier: 
     hierarchical_identifier
;

hierarchical_task_identifier:
     hierarchical_identifier
;

IDENTIFIER:		
     SIMPLE_IDENTIFIER
   | ESCAPED_IDENTIFIER
;

inout_port_identifier:
     IDENTIFIER
;

input_port_identifier:
     IDENTIFIER
;

instance_identifier:
     IDENTIFIER
;

library_identifier:
     IDENTIFIER
;

module_identifier:
     IDENTIFIER
;

module_instance_identifier:
     IDENTIFIER
;

net_identifier:
     IDENTIFIER
;

output_port_identifier:
     IDENTIFIER
;

parameter_identifier:
     IDENTIFIER
;

port_identifier:
     IDENTIFIER
;

real_identifier:
     IDENTIFIER
;

fragment
SIMPLE_IDENTIFIER:
     ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '0'..'9' | '_' | '$')*
;

specparam_identifier:
     IDENTIFIER
;

system_function_identifier:
     SYSTEM_IDENTIFIER
;

system_task_identifier:
     SYSTEM_IDENTIFIER
;

SYSTEM_IDENTIFIER:                                // Extra rule for system_task and system_function identifiers
     '$' ('a'..'z' | 'A'..'Z' | '0'..'9' | '_' | '$')+
;

task_identifier:
     IDENTIFIER
;

topmodule_identifier:
     IDENTIFIER
;

udp_identifier:
     IDENTIFIER
;

udp_instance_identifier:
     IDENTIFIER
;

variable_identifier:
     IDENTIFIER
;





// ------------------------------------------------------
// A.9.4 White space

WS:
     (' ' | '\t' | '\r' | '\n')+ { $channel=HIDDEN; }
;







// --------------------------------------------------------------------------
// My rules

file_path_spec:     // TODO: damn this sucks
     '*.*'
;







